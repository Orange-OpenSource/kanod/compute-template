/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ComputeTemplatePrefix = "compute-template.kanod.io"
	ComputeSecretVersion  = "compute-template.kanod.io/version"
	TemplateFieldName     = "template"
)

// ComputeTemplateSpec defines the desired state of ComputeTemplate
type ComputeTemplateSpec struct {
	// Template holds the name of the secret containing the actual template.
	Template string `json:"template"`
	// Field is the name of the field in the generated secret after template expansion
	Field string `json:"field"`
	// Suffix is the suffix appended to the name of the compute resource to create the name of secret.
	// If not provided -<name> where <name> is the name of the ComputeTemplate is used.
	Suffix string `json:"suffix"`
	// Delimiters defines a comma separated pair of optional delimiter
	Delimiters string `json:"delimiters,omitempty"`
	// FromLabels associate keys to values of some labels in the BaremetalHost
	FromLabels []FromLabel `json:"fromLabels,omitempty"`
	// FromLabels associate keys to values of some labels in the BaremetalHost
	FromAnnotations []FromAnnotation `json:"fromAnnotations,omitempty"`
}

// FromLabel represents how a label of the resource is mapped to a key for
// template processing
type FromLabel struct {
	// Key is the name of the element in the templates
	Key string `json:"key"`
	// Label is the name of the label on the BareMetalHost
	Label string `json:"label"`
	// Default is a default value used if the label is not defined
	Default *string `json:"default,omitempty"`
}

// FromLabel represents how an annotation of the resource is mapped to a key for
// template processing
type FromAnnotation struct {
	// Key is the name of the element in the templates
	Key string `json:"key"`
	// Annotation is the name of the label on the BareMetalHost
	Annotation string `json:"annotation"`
	// Default is a default value used if the annotation is not defined
	Default *string `json:"default,omitempty"`
}

// ComputeTemplateStatus defines the observed state of ComputeTemplate
type ComputeTemplateStatus struct {
	// Parsable indicates that the template respect the basic structure.
	Parsable bool `json:"parsable"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ComputeTemplate is the Schema for the computetemplates API
type ComputeTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ComputeTemplateSpec   `json:"spec,omitempty"`
	Status ComputeTemplateStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ComputeTemplateList contains a list of ComputeTemplate
type ComputeTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ComputeTemplate `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ComputeTemplate{}, &ComputeTemplateList{})
}
