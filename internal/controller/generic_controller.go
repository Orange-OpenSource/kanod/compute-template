package controller

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	texttemplate "text/template"

	sprig "github.com/go-task/slim-sprig"
	compute "gitlab.com/Orange-OpenSource/kanod/compute-template/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logr "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

type GenericComputeReconciler struct {
	client.Client
	GVK        schema.GroupVersionKind
	Label      string
	Field      string
	UpdatePath string
}

func (r *GenericComputeReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = logr.FromContext(ctx)
	namespace := req.Namespace
	ctrlName := strings.ToLower(r.GVK.Kind) + "-controller"
	log := logr.Log.WithName(ctrlName).WithValues("namespace", namespace, "name", req.Name)
	resource := &unstructured.Unstructured{}
	resource.SetGroupVersionKind(r.GVK)

	err := r.Get(ctx, req.NamespacedName, resource)
	if err != nil {
		log.Error(err, "missing object")
		return ctrl.Result{}, err
	}
	labels := resource.GetLabels()
	if labels == nil {
		return ctrl.Result{}, nil
	}
	templateName, ok := labels[r.Label]
	if !ok {
		return ctrl.Result{}, nil
	}
	log.Info("Found label with value", "value", templateName)

	// Get Compute Template
	template := &compute.ComputeTemplate{}
	templateKey := client.ObjectKey{Namespace: namespace, Name: templateName}
	if err = r.Get(ctx, templateKey, template); err != nil {
		log.Error(err, "Missing template", "templateName", templateName)
		return ctrl.Result{}, err
	}
	// Compute Secret Name.
	var secretName string
	var secret = &corev1.Secret{}
	if template.Spec.Suffix != "" {
		secretName = req.Name + template.Spec.Suffix
	} else {
		secretName = req.Name + "-" + templateName
	}
	// Get Secret
	secretKey := client.ObjectKey{Namespace: namespace, Name: secretName}
	create := false
	if err = r.Get(ctx, secretKey, secret); err != nil {
		if !errors.IsNotFound(err) {
			log.Error(err, "Cannot get ", "templateName", templateName)
			return ctrl.Result{}, err
		}
		create = true
	} else {
		annotations := secret.Annotations
		if annotations != nil {
			version, ok := annotations[compute.ComputeSecretVersion]
			if ok && version == template.ResourceVersion {
				log.V(1).Info("Already generated for this version")
				return ctrl.Result{}, nil
			}
		}
	}

	// Get Template Secret
	var templateSecret = &corev1.Secret{}
	templateSecretKey := client.ObjectKey{Namespace: namespace, Name: template.Spec.Template}
	if err = r.Get(ctx, templateSecretKey, templateSecret); err != nil {
		log.Error(
			err, "Missing template secret", "templateName", templateName,
			"secretName", template.Spec.Template)
		return ctrl.Result{}, err
	}
	templateText, ok := templateSecret.Data[compute.TemplateFieldName]
	if !ok {
		log.Error(
			fmt.Errorf("missing template field"),
			"Template field not found in secret", "secretName", template.Spec.Template)
	}
	println(templateText)
	// Generate Secret
	templateContext, err := computeTemplateContext(template, resource)
	if err != nil {
		log.Error(err, "Missing meta data in the resource")
	}

	log.V(1).Info("Parsing template")
	var tpl = texttemplate.New(templateName)
	if template.Spec.Delimiters != "" {
		delims := strings.Split(template.Spec.Delimiters, ",")
		if len(delims) == 2 {
			tpl = tpl.Delims(delims[0], delims[1])
		}
	}
	tmpl, err := tpl.Funcs(sprig.HermeticTxtFuncMap()).Option("missingkey=error").
		Parse(string(templateText))
	if err != nil {
		log.Error(
			err, "Cannot parse template",
			"computeTemplateName", template.Name, "secretName", template.Spec.Template)
		return ctrl.Result{}, err
	}
	log.V(1).Info("Rendering template")
	var buffer bytes.Buffer
	err = tmpl.Execute(&buffer, templateContext)
	if err != nil {
		log.Error(
			err, "Cannot render template",
			"template", string(templateText),
			"context", templateContext)
		return ctrl.Result{}, err
	}
	// Create/Update secret
	if secret.Data == nil {
		secret.Data = map[string][]byte{}
	}
	secret.Data[r.Field] = buffer.Bytes()
	if create {
		err = r.Create(ctx, secret)
	} else {
		err = r.Update(ctx, secret)
	}
	if err != nil {
		log.Error(err, "cannot create/update the target secret.")
		return ctrl.Result{}, err
	}
	if r.UpdatePath != "" {
		b, err := patchResource(resource, r.UpdatePath, secretName)
		if err != nil {
			log.Error(err, "cannot patch the resource.", "path", r.UpdatePath)
			return ctrl.Result{}, err
		}
		if b {
			err := r.Update(ctx, resource)
			if err != nil {
				log.Error(err, "Updating the secret in the resource failed.", "path", r.UpdatePath)
				return ctrl.Result{}, err
			}
		}
	}
	return ctrl.Result{}, nil
}

// Patch an unstructured resource by setting the field at path to value.
//
// The path is sepecified as a json pointer a list of fields separated by slashes
// with special values ~0 and ~1 for respectively tilde and slash. The result is a pair boolean
// and error where the boolean tells if the value of the field was modified (true) or if it
// was already the value provided (false).

func patchResource(resource *unstructured.Unstructured, path string, value string) (bool, error) {
	var obj map[string]interface{} = resource.Object
	fields := strings.Split(path, "/")
	// Sanitize the paths
	for i, field := range fields {
		fields[i] = strings.ReplaceAll(strings.ReplaceAll(field, "~1", "/"), "~0", "~")
	}
	last := len(fields) - 1
	for i, field := range fields {
		if i == last {
			modified := (obj[field] != value)
			obj[field] = value
			return modified, nil
		} else {
			subobj, ok := obj[field]
			if !ok {
				return false, fmt.Errorf("missing field %s", strings.Join(fields[0:i+1], "."))
			}
			obj, ok = subobj.(map[string]interface{})
			if !ok {
				return false, fmt.Errorf("bad field %s is not an object", strings.Join(fields[0:i+1], "."))
			}
		}
	}
	// can only occur with an empty path
	return false, nil
}

func (r *GenericComputeReconciler) SetupWithManager(mgr ctrl.Manager) error {
	u := &unstructured.Unstructured{}
	u.SetGroupVersionKind(r.GVK)
	name := strings.ToLower(r.GVK.Kind) + "-controller"
	log := logr.Log.WithName(name)
	c, err := controller.New(name, mgr, controller.Options{
		Reconciler: reconcile.Func(r.Reconcile)})
	if err != nil {
		log.Error(err, "unable to create controller for compute resource")
		return err
	}

	pred, err := predicate.LabelSelectorPredicate(
		metav1.LabelSelector{
			MatchLabels: map[string]string{},
			MatchExpressions: []metav1.LabelSelectorRequirement{
				{Key: r.Label, Operator: metav1.LabelSelectorOpExists},
			},
		},
	)
	if err != nil {
		log.Error(err, "label selector problem")
		return err
	}
	err = c.Watch(
		source.Kind(mgr.GetCache(), u),
		&handler.EnqueueRequestForObject{},
		pred,
	)
	if err != nil {
		log.Error(err, "unable to watch compute resource")
		return err
	}
	return nil
}

func computeTemplateContext(
	computeTemplate *compute.ComputeTemplate,
	resource metav1.Object,

) (map[string]interface{}, error) {
	var computeTemplateContext = map[string]interface{}{}
	labels := resource.GetLabels()
	if labels != nil {
		for _, elt := range computeTemplate.Spec.FromLabels {
			v, ok := labels[elt.Label]
			if ok {
				computeTemplateContext[elt.Key] = v
			} else {
				if elt.Default != nil {
					computeTemplateContext[elt.Key] = *elt.Default
				} else {
					return nil, fmt.Errorf("missing label %s", elt.Label)
				}
			}
		}
	}
	annotations := resource.GetAnnotations()
	if annotations != nil {
		for _, elt := range computeTemplate.Spec.FromAnnotations {
			v, ok := annotations[elt.Annotation]
			if ok {
				computeTemplateContext[elt.Key] = v
			} else {
				if elt.Default != nil {
					computeTemplateContext[elt.Key] = *elt.Default
				} else {
					return nil, fmt.Errorf("missing annotation %s", elt.Annotation)
				}
			}
		}
	}
	computeTemplateContext["name"] = resource.GetName()
	computeTemplateContext["namespace"] = resource.GetNamespace()
	return computeTemplateContext, nil
}
